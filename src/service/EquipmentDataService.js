import http from '../http-common';
import Vue from 'vue'


class EquipmentDataService   {
    getAllEquipment() {
        return http.instance.get('/Equipment')
    }

    getEquipmentById(id) {
        return http.instance.get(`/Equipment/${id}`)
    }

    searchEquipment(data) {
        return http.instance.get(`/Equipment/search?name=${data}`)
    }


    createEquipment(data) {
        return http.instance.post('/Equipment/', data)
    }

    updateEquipment(id, data) {
        return http.instance.put(`/Equipment/${id}`, data)
    }

    
    deleteEquipment(id) {
        return http.instance.delete(`/Equipment/${id}`)
    }


    getButterData(pageType) {
        return http.butterInstance.get(`/pages/${pageType}?auth_token=c60a9ee5329e50806b293f77216f0d9422b139c6`)
    }

    getButterPage(pageType, slug) {
        return http.butterInstance.get(`/pages/${pageType}/${slug}?auth_token=c60a9ee5329e50806b293f77216f0d9422b139c6`)

    }

    getButterPageByCategory(pageType, story) {
        return http.butterInstance.get(`pages/${pageType}/?fields.stories.slug=${story}&auth_token=c60a9ee5329e50806b293f77216f0d9422b139c6`)
    }
    
    getCollections(story) {
        return http.butterInstance.get(`content/${story}/?auth_token=c60a9ee5329e50806b293f77216f0d9422b139c6`)
       
    }

    showMessage(title, message, messageType) {
        Vue.notify({
          group: "notify",
          title: title,
          text: message,
          type: messageType
        });
      }
}

export default new EquipmentDataService();