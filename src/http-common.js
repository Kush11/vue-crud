import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'



axios.defaults.headers.post["content-type"] = "application/json";

const instance = axios.create({
  baseURL: "http://etestapi.test.eminenttechnology.com/api",
  // headers: {
  //   "content-type": "application/json"
  // }
});

const butterInstance = axios.create({
  baseURL: "https://api.buttercms.com/v2",
  // headers: {
  //   "content-type": "application/json"
  // }
});



instance.interceptors.request.use(config => {
  NProgress.start()
  return config
})

// before a response is returned stop nprogress
instance.interceptors.response.use(response => {
  NProgress.done()
  return response
})

butterInstance.interceptors.request.use(config => {
  NProgress.start()
  return config
})

// before a response is returned stop nprogress
butterInstance.interceptors.response.use(response => {
  NProgress.done()
  return response
})

// export default instance;
// export default butterInstance

export default {
  instance,
  butterInstance
}