import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import EquipmentDataService from "../service/EquipmentDataService";

export default new Vuex.Store({
    state: {
        equiptments: [],
        isUserLoggedIn: false
    },

    mutations: {
        GET_EQUIPMENT(state, equiptments) {
            state.equiptments = equiptments
        },

        SEARCH_EQUIPMENT(state, equiptments) {
            state.equiptments = equiptments
        },
        AUTH_USER(state, isUserLoggedIn) {
            state.isUserLoggedIn = isUserLoggedIn
            console.log(state.isUserLoggedIn)

        }


        // ADD_EQUIPMENT(state) {   
        //     console.log(state)    

        // },
        // EDIT_EQUIPMENT(state, equiptment){
        //     this.$store.dispatch("getEquipment");  
        //     this.$emit("close");

        //     var equiptments = state.equiptments
        //     equiptments.splice(equiptments.indexOf(equiptment), 1)
        // },
        // REMOVE_EQUIPMENT(state) {
        //     this.$store.dispatch("getEquipment"); 
        //     this.$emit("close");

        //     state.newEquiptment = ''
        // },


    },

    actions: {
        getEquipment({ commit }) {
            EquipmentDataService.getAllEquipment()
                .then(response => {
                    commit('GET_EQUIPMENT', response.data)
                })
        },

        searchEquipment({ commit }, equipmentName) {
            EquipmentDataService.searchEquipment(equipmentName)
                .then(response => {
                    commit('SEARCH_EQUIPMENT', response.data)
                })
        },

        login({ commit }, isUserLoggedIn) {
            commit('AUTH_USER', isUserLoggedIn)

        }

        // addEquipment({commit}, equipment) {
        //     EquipmentDataService.createEquipment(equipment)
        //     .then(response => {
        //         this.$store.dispatch('getEquipment')
        //         commit('ADD_EQUIPMENT', response.data)
        //     })
        // },
        // editEquipment({commit}, equiptment) {
        //     EquipmentDataService.editEquipment(equipment.id, equiptment)
        //     .then(response => {
        //         commit('EDIT_EQUPMENT', response.data)
        //     })
        // },
        // removeEquipment({commit}, equipment) {
        //     EquipmentDataService.deleteEquipment(equipment.id)
        //     .then(response => {
        //         commit('REMOVE_EQUIPMENT', response.data)
        //     })
        // }
    },

    getters: {
        newEquiptment: state => state.newEquiptment,
        equipments: (state) => {
            return state.equiptments
        },

        isLoggedIn: (state) => {
            return state.isUserLoggedIn
        }
    }
})