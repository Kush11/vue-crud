import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import router from './router'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import Notifications from 'vue-notification'
import { ValidationProvider, extend } from 'vee-validate';
import { required, min_value } from 'vee-validate/dist/rules';
import Butter from 'buttercms';

// import JwPagination from 'jw-vue-pagination';




// Vue.component('jw-pagination', JwPagination);
Vue.component('ValidationProvider', ValidationProvider);


Vue.use(Vuelidate);
Vue.use(VueRouter);
Vue.use(Notifications)

extend('min_value', {
  ...min_value,
  message: 'Value can not be less than 1'
});
extend('required', {
  ...required,
  message: 'This field is required'
});



extend('required', {
  ...required,
  message: 'This field is required'
});


const HelloJs = require('hellojs/dist/hello.all.min.js');
const VueHello = require('vue-hellojs');
// const router = new VueRouter({});



HelloJs.init({
  google: '408887561229-s4btg63f7run7mjv31o287suuur8p7af.apps.googleusercontent.com'
  //facebook: FACEBOOK_APP_CLIENT_ID
}, {
  redirect_uri: 'authcallback/'
});


Vue.use(VueHello, HelloJs);
Vue.config.productionTip = false


export const bus = new Vue();
export const butter = new Butter('c60a9ee5329e50806b293f77216f0d9422b139c6')
export default bus;


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
