import Vue from 'vue'
import Router from 'vue-router'
import EquipmentGrid from './components/EquipmentGrid.vue'
import Auth from './components/Authenticate.vue'
import ButterCMS from './components/ButterCMS.vue'
import ButterCmsDetail from './components/ButterCmsDetail.vue'




Vue.use(Router);

function guardRoute(to, from, next) {
    var userToken = JSON.parse(localStorage.getItem('hello'))
    if (!userToken) {
        next('/login')
    } else {
        next()
    }
}
export default new Router({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'equipments',

            alias: '/equipments',
            beforeEnter: guardRoute,
            component: EquipmentGrid,
           
        },

        {
            path: '/login',
            name: 'login',
            component: Auth
        },

        {
            path: '/butter',
            name: 'butter',
            component: ButterCMS
        },
        {
            path: '/butterDetail/:slug',
            name: 'butterDetail',
            component: ButterCmsDetail
        },
    ]
})